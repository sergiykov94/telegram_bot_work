import json
import logging
from aiogram import Bot, Dispatcher, executor, types
from aiogram.utils.markdown import hbold, hunderline, hcode, hlink
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton
from aiogram.dispatcher.filters import Text
from main import check_work_update
from setting import TOKEN

#log level
logging.basicConfig(level=logging.INFO)

#bot init
bot = Bot(token=TOKEN, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot)

@dp.message_handler(commands=['start'])
async def start(message: types.Message):
    start_buttons = ['Вакансії', 'Вакансії з зарплатою', 'Перевірити нові вакансії']
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*start_buttons)
    await message.answer("Парсинг сайту rabota.ua Junior Python Dev Ukraine", reply_markup=keyboard)

@dp.message_handler(Text(equals="Вакансії"))
async def get_list_work(message: types.Message):
    with open("work_dict.json") as file:
        work_dict = json.load(file)

    for k, v in work_dict.items():
        works = f"{hunderline(v['work_company_title'])}\n" \
                f"{v['work_company_name']}\n" \
                f"{v['work_company_location']}\n" \
                f"{v['work_company_salary']}\n" \
                f"{v['work_url']}\n" 
        await message.answer(works)

@dp.message_handler(Text(equals="Вакансії з зарплатою"))
async def get_list_work(message: types.Message):
    with open("work_dict.json") as file:
        work_dict = json.load(file)

    for k, v in work_dict.items():
        if f"{v['work_company_salary']}" is not "":
            works = f"{v['work_company_title']}\n" \
                        f"{v['work_company_name']}\n" \
                        f"{v['work_company_location']}\n" \
                        f"{v['work_company_salary']}\n" \
                        f"{v['work_url']}\n" 
            await message.answer(works)

@dp.message_handler(Text(equals="Перевірити нові вакансії"))
async def check_new_work_update(message: types.Message):
    fresh_works = check_work_update()

    if fresh_works:
        for k, v in fresh_works.items():
            works = f"{hunderline(v['work_company_title'])}\n" \
                    f"{v['work_company_name']}\n" \
                    f"{v['work_company_location']}\n" \
                    f"{v['work_company_salary']}\n" \
                    f"{v['work_url']}\n" 

            await message.answer(works)
    if fresh_works is None:
        await message.answer("Нових вакансій немає...")

if __name__=="__main__":
    executor.start_polling(dp, skip_updates=True)
