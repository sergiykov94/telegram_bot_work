import json
import requests
from setting import URL, HEADERS
from bs4 import BeautifulSoup

def get_work():
    """
        Function should parse 1 page from rabota.ua
    """
    r = requests.get(url=URL, headers=HEADERS)
    soup = BeautifulSoup(r.text, "lxml")
    work_cards = soup.find_all("article", class_="card")

    work_dict = {}
    for work in work_cards:
        work_company_title = work.find("a", class_="ga_listing").text.strip()
        work_company_name = work.find("a", class_="company-profile-name").text.strip()
        work_company_location = work.find("span", class_="location").text.strip()
        work_company_salary = work.find("span", class_="salary").text.strip()
        work_company_description = work.find("div", class_="card-description").text.strip()
        work_url = work.find("a", class_="ga_listing").get("href")
        # Create a unique id in json file from work_url  exp 8600164
        url_id = work_url.split("y")[-1]

        work_dict[url_id] = {
            "work_company_title" : work_company_title,
            "work_company_name" : work_company_name,
            "work_company_location" : work_company_location,
            "work_company_salary" : work_company_salary,
            "work_company_description" : work_company_description,
            "work_url" : f'https://rabota.ua{work_url}',
        }
    # Create json file with work_dict information
    with open("work_dict.json", "w") as file:
        json.dump(work_dict, file, indent=4, ensure_ascii=False)

def check_work_update():
    """
        Function should check updates, if yes re write json file
    """
    # Open jsno file
    with open("work_dict.json") as file:
        work_dict = json.load(file)

    r = requests.get(url=URL, headers=HEADERS)
    soup = BeautifulSoup(r.text, "lxml")
    work_cards = soup.find_all("article", class_="card")

    fresh_work = {}
    for work in work_cards:
        work_url = work.find("a", class_="ga_listing").get("href")
        url_id = work_url.split("y")[-1]
        # import pdb; pdb.set_trace()

        if url_id in work_dict:
            continue
            print('Нету новых вакансий')

        work_company_title = work.find("a", class_="ga_listing").text.strip()
        work_company_name = work.find("a", class_="company-profile-name").text.strip()
        work_company_location = work.find("span", class_="location").text.strip()
        work_company_salary = work.find("span", class_="salary").text.strip()
        work_company_description = work.find("div", class_="card-description").text.strip()

        work_dict[url_id] = {
            "work_company_title" : work_company_title,
            "work_company_name" : work_company_name,
            "work_company_location" : work_company_location,
            "work_company_salary" : work_company_salary,
            "work_company_description" : work_company_description,
            "work_url" : f'https://rabota.ua{work_url}',
        }

        # Write new vacancies
        fresh_work[url_id] = {
            "work_company_title" : work_company_title,
            "work_company_name" : work_company_name,
            "work_company_location" : work_company_location,
            "work_company_salary" : work_company_salary,
            "work_company_description" : work_company_description,
            "work_url" : f'https://rabota.ua{work_url}',
        }

        with open("work_dict.json", "w") as file:
            json.dump(work_dict, file, indent=4, ensure_ascii=False)

        return fresh_work

def main():
    check_work_update()
    # get_work()


if __name__ == '__main__':
    main()
